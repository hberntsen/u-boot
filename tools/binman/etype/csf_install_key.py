# SPDX-License-Identifier: GPL-2.0+
# Written by Harm Berntsen <harm.berntsen@nedap.com>

"""Entry-type module for the key insertion command in a CSF file

This generates the key installation command data for a CSF file
"""

from binman.etype.csf_header import CsfHeader
from binman.entry import Entry
from dtoc import fdt_util
from ctypes import *

HAB_CMD_INS_KEY = 0xbe
HAB_CMD_INS_KEY_CSF = 0x2
HAB_PCL_X509 = 0x9
HAB_TAG_CRT = 0xd7

class CsfCmdInsKey(BigEndianStructure):
    _pack_ = 1
    _fields_ = [
            ("header", CsfHeader),
            ("cert_fmt", c_uint8),
            ("hash_alg", c_uint8),
            ("src_index", c_uint8),
            ("tgt_index", c_uint8),
            ("key_loc", c_uint32),
            ]

class Entry_csf_install_key(Entry):
    """CSF command to install a key

    Properties / Entry arguments:
        - attachment: phandle to the attachment Entry
        - tgt-index: key index into which the public key is loaded. Default: 0
        - csfk:
            when present, marks the key as the key to authenticate this CSF"""
    def __init__(self, section, etype, node):
        super().__init__(section, etype, node)
        self.cmd = CsfCmdInsKey()
        self.cmd.header.tag = HAB_CMD_INS_KEY
        self.cmd.header.length = len(bytes(self.cmd))
        self.cmd.cert_fmt = HAB_PCL_X509

    def ReadNode(self):
        super().ReadNode()
        self.cmd.tgt_index = fdt_util.GetInt(self._node, 'tgt-index', 0)

        self.attachment_phandle = fdt_util.GetPhandleList(self._node, 'attachment')
        if not self.attachment_phandle or not self.attachment_phandle[0]:
            self.Raise("Missing attachment phandle")
        self.attachment_phandle = self.attachment_phandle[0]

        if 'csfk' in self._node.props:
            self.cmd.header.par = HAB_CMD_INS_KEY_CSF

    def _UpdateKeyLoc(self):
        attachment_entry = self.section.GetEntryByPhandle(self.attachment_phandle, self)
        self.cmd.key_loc = attachment_entry.offset or 0

    def ObtainContents(self):
        self._UpdateKeyLoc()
        self.SetContents(bytes(self.cmd))
        return True

    def ProcessContents(self):
        self._UpdateKeyLoc()
        return self.ProcessContentsUpdate(bytes(self.cmd))
