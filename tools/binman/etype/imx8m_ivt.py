# SPDX-License-Identifier: GPL-2.0+
# Written by Harm Berntsen <harm.berntsen@nedap.com>

"""Entry-type module for generating an i.MX8MN IVT header

This module creates an IVT header, suitable for an i.MX8MN board that boots via
download mode, internal eMMC or MicroSD.
"""

from binman.entry import Entry
from dtoc import fdt_util
from ctypes import *

# This is imported if needed
state = None

# Based on imximage.h
class IvtHeader(BigEndianStructure):
    _pack_ = 1
    _fields_ = [
            ("tag", c_uint8),
            ("length", c_uint16),
            ("version", c_uint8)
            ]

class FlashHeaderV2(LittleEndianStructure):
    _pack_ = 1
    _fields_ = [
            ("header", IvtHeader),
            ("entry", c_uint32),
            ("reserved1", c_uint32),
            ("dcd_ptr", c_uint32),
            ("boot_data_ptr", c_uint32),
            ("self", c_uint32),
            ("csf", c_uint32),
            ("reserved2", c_uint32)
            ]

class BootData(LittleEndianStructure):
    _pack_ = 1
    _fields_ = [
            ("start", c_uint32),
            ("size", c_uint32),
            ("plugin", c_uint32)
            ]

class ImxHeaderV3(LittleEndianStructure):
    _pack_ = 1
    _fields_ = [
            ("fhdr", FlashHeaderV2),
            ("boot_data", BootData),
            ]


IVT_HEADER_TAG = 0xD1
IVT_VERSION_V3 = 0x41

class Entry_imx8m_ivt(Entry):
    """i.MX8M IVT header

    Properties / Entry arguments:
        - entrypoint
            entrypoint address for the chip (usually CONFIG_SPL_TEXT_BASE)
        - loader
            phandle to the U-Boot SPL + DDR firmware section. Should be placed
            directly after this IVT header in the resulting image
        - csf
            phandle to the csf entry. Needs to be placed directly after the
            loader in the resulting binary

    This Entry should be the first entry in the image and generates the IVT
    header for booting U-Boot SPL."""
    def __init__(self, section, etype, node):
        # Put this here to allow entry-docs and help to work without libfdt
        global state
        from binman import state

        super().__init__(section, etype, node)

        self.imx_header = ImxHeaderV3()
        # Based on imx8mimage.c
        self.imx_header.fhdr.header.tag = IVT_HEADER_TAG
        self.imx_header.fhdr.header.length = len(bytes(self.imx_header.fhdr))
        self.imx_header.fhdr.header.version = IVT_VERSION_V3
        self.csf_entry = None

    def ReadNode(self):
        super().ReadNode()
        entrypoint = fdt_util.GetInt(self._node, 'entrypoint')
        if not entrypoint:
            self.Raise("missing 'entrypoint' property");
        self.imx_header.fhdr.entry = entrypoint
        self.Detail('entry=0x%x' % self.imx_header.fhdr.entry)

        self.imx_header.fhdr.self = self.imx_header.fhdr.entry - \
                len(bytes(self.imx_header))
        self.Detail('self=0x%x' % self.imx_header.fhdr.self)

        self.imx_header.fhdr.boot_data_ptr = self.imx_header.fhdr.self + \
                ImxHeaderV3.boot_data.offset
        self.Detail('boot_data_ptr=0x%x' % self.imx_header.fhdr.boot_data_ptr)

        self.imx_header.boot_data.start = self.imx_header.fhdr.self
        self.Detail('boot_data.start=0x%x' % self.imx_header.boot_data.start)

    def ProcessFdt(self, fdt):
        if not super().ProcessFdt(fdt):
            return False

        csf_phandles = fdt_util.GetPhandleList(self._node, 'csf')
        if csf_phandles:
            image = self.GetImage()
            self.csf_entry = image.GetEntryByPhandle(csf_phandles[0], self)

            if not self.csf_entry:
                return False

        if not fdt_util.GetInt(self._node, 'load'):
            load = self.imx_header.fhdr.entry - len(bytes(self.imx_header))
            state.AddInt(self._node, 'load', load)

        return True

    def _UpdateBootDataSize(self):
        loader_phandles = fdt_util.GetPhandleList(self._node, 'loader')
        if not loader_phandles:
            self.Raise("missing 'loader' property")
        ldr = self.section.GetContentsByPhandle(loader_phandles[0], self, True)
        self.imx_header.boot_data.size = len(ldr) + len(bytes(self.imx_header))

        if self.csf_entry:
            if self.csf_entry.offset != self.imx_header.boot_data.size:
                # not sure what the real hardware limitations are, we assume
                # the following holds in caluclating the 'load' property in
                # the AddMissingProperties function
                self.Raise("csf should be placed immediately after loader "
                           f"(0x{self.csf_entry.offset:x} vs "
                           f"0x{self.imx_header.boot_data.size:x})")
            self.imx_header.boot_data.size += self.csf_entry.size

        self.Detail('boot_data.size=0x%x' % self.imx_header.boot_data.size)

    def _UpdateCsf(self):
        if self.csf_entry:
            csf = self.imx_header.fhdr.self + self.csf_entry.offset
            self.imx_header.fhdr.csf = csf
        self.Detail('fhdr.csf=0x%x' % self.imx_header.fhdr.csf)

    def ProcessContents(self):
        self._UpdateBootDataSize()
        self._UpdateCsf()

        return self.ProcessContentsUpdate(bytes(self.imx_header))

    def ObtainContents(self):
        self.SetContents(bytes(self.imx_header))
        return True
