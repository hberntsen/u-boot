# SPDX-License-Identifier: GPL-2.0+
# Written by Harm Berntsen <harm.berntsen@nedap.com>

"""Entry-type module for attaching signatures to CSF commands."""

from binman.etype.csf_header import CsfHeader
from binman.entry import Entry
from dtoc import fdt_util
from ctypes import sizeof
from patman import tools
from cryptography import x509
from typing import List, Optional, Tuple
from patman import tout

HAB_TAG_SIG = 0xd8

# This is imported if needed
state = None

def GetDataToAuthenticate(
        calling_entry: Entry,
        authenticate_data_phandles: List[int]) -> List[Tuple[Optional[int], bytes]]:
    """Generates a list of regions to authenticate

    Uses the 'load' property to infer where data is loaded into memory and
    whether regions can be collapsed into a single region. Automatically
    infers the load address for consecutive entries from the first.

    Args:
        calling_entry: Entry
            The entry which requires this data
        authenticate_data_phandles: List[int]
            List of phandles of data to be authenticated, ideally specified in
            the order they are loaded into memory

    Returns:
        a list of tuples containing optionally the address at which the data
        will be loaded and the data itself
    """
    regions = []
    prev_entry = None
    image = calling_entry.GetImage()

    for nth, phandle in enumerate(authenticate_data_phandles):
        # First, try obtaining the load address from the entry itself
        entry = image.GetEntryByPhandle(phandle, calling_entry, True)
        if entry is None:
            calling_entry.Raise(f"Cannot find entry for phandle #{nth}")
        entry.AddMissingProperties(False)
        load = fdt_util.GetInt(entry._node, 'load')
        # Otherwise, infer it from the previous entry. Useful for things like
        # the CSF section that does not have a load address, whilst the
        # previous u-boot-spl-ddr section has
        if load == None and \
            prev_entry and \
            prev_entry.offset is not None and \
            prev_entry.offset + prev_entry.contents_size == entry.offset and \
            regions[-1][0] is not None:

            load = regions[-1][0] + len(regions[-1][1])
            calling_entry.Detail(f"Assuming {entry._node.path} is loaded " +
                f"immediately after {prev_entry._node.path}: 0x{load:02x}")

        data = entry.GetData(False)
        if data and entry.contents_size:
            data = data[:entry.contents_size]

        # Compact the number of regions
        if regions and \
                regions[-1][0] is not None and regions[-1][1] is not None and \
                load == regions[-1][0] + len(regions[-1][1]):

            if regions[-1][1] is not None and data is not None:
                regions[-1] = (regions[-1][0], regions[-1][1] + data)
        else:
            regions.append((load,data))

        prev_entry = entry

    return regions

class Entry_csf_authenticate_attachment(Entry):
    """Base class to hold signature data

    Do not use this class directly. See csf-authenticate-attachment-keyfile
    and csf-authenticate-attachment-pkcs11.

    Properties / Entry arguments:
        - hab-version: byte that indicates the HAB version used (0xVv with V
            the major version and v the minor version). Defaults to 0x45
        - crt: certificate file name. Has to be loaded into key-index by an
            earlier command
        - sign: list of phandles to sign (ideally in order they are loaded
            into memory)"""

    def __init__(self, section, etype, node):
        # Put this here to allow entry-docs and help to work without libfdt
        global state
        from binman import state

        super().__init__(section, etype, node)

        self.header = CsfHeader()
        self.header.tag = HAB_TAG_SIG

    def ReadNode(self):
        self.header.par = fdt_util.GetByte(self._node, 'hab-version', 0x45)

        specified_crt_path = fdt_util.GetString(self._node, 'crt') or \
                self.Raise("No crt path specified")
        crt_path = tools.get_input_filename(specified_crt_path, True)
        if crt_path is None:
            self.missing = True
            tout.warning(f"Missing {specified_crt_path}, resulting CSF will " +
                    "not be signed")
        else:
            self.crt = x509.load_der_x509_certificate(tools.read_file(crt_path))
            self.sign_phandles = fdt_util.GetPhandleList(self._node, 'sign') or []
            self._bytes_to_authenticate = None

        if not 'align-size' in self._node.props:
            state.AddInt(self._node, 'align-size', 4)
        super().ReadNode()

    def GetSignature(self) -> Optional["bytes"]:
        d2a = GetDataToAuthenticate(self, self.sign_phandles)
        for (load, data) in d2a:
            if not data:
                return None

        bytes_to_authenticate = b''.join([data for (_, data) in d2a])

        if len(bytes_to_authenticate) == 0:
            self.Raise("Refusing to authenticate 0 bytes")

        if self._bytes_to_authenticate != bytes_to_authenticate:
            self.Detail("Signing %i bytes" % len(bytes_to_authenticate))
            # Subclasses have _Sign functions that do not always return None
            # so disable that pylint warning here
            # pylint: disable=E1128
            self._signature = self._Sign(bytes_to_authenticate)
            if self._signature is None:
                return None
            # Cache the last data we signed to prevent we re-sign the same
            # data
            self._bytes_to_authenticate = bytes_to_authenticate

        return self._signature

    def _Sign(self, data: "bytes") -> Optional["bytes"]:
        self.Raise("The CSF authenticate attachment base class cannot sign")
        return None

    def ObtainContents(self):
        if self.missing:
            self.SetContents(bytes(self.header))
            return True
        else:
            signature = self.GetSignature()
            if signature is None:
                return False

            self.header.length = sizeof(CsfHeader) + len(signature)
            self.SetContents(bytes(self.header) + signature)

        return True

    def ProcessContents(self):
        if self.missing:
            return True

        data = bytes(self.header) + self.GetSignature()
        return self.ProcessContentsUpdate(data)
