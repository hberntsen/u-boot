# SPDX-License-Identifier: GPL-2.0+
# Written by Harm Berntsen <harm.berntsen@nedap.com>

"""Entry-type module for attaching blobs that belong to CSF commands at the end
of the CSF
"""

from binman.etype.csf_header import CsfHeader
from binman.etype.blob_ext import Entry_blob_ext
from dtoc import fdt_util
from ctypes import sizeof

# This is imported if needed
state = None

class Entry_csf_file_attachment(Entry_blob_ext):
    """Attach external blobs to a CSF

    Properties / Entry arguments:
        - tag
            byte to start the blob with, i.e. [d7] for a certificate. See High
            Assurance Boot Version 4 Application Programming Interface
            Reference Manual (included in NXP's Code Signing Tool), Section
            6.2 for a list of possible tags.
        - hab-version
            byte that indicates the HAB version used (0xVv with V the major
            version and v the minor version). Defaults to 0x45
        - filename: Filename of the file to read into this attachment

    Used for attaching external blobs to the CSF. Prefixes the blob with a
    CSF header."""
    def __init__(self, section, etype, node):
        # Put this here to allow entry-docs and help to work without libfdt
        global state
        from binman import state

        super().__init__(section, etype, node)
        self.header = CsfHeader()

    def ReadNode(self):
        self.header.tag = fdt_util.GetByte(self._node, 'tag') \
                or self.Raise("Missing tag")
        self.header.par = fdt_util.GetByte(self._node, 'hab-version', 0x45)

        if not 'align-size' in self._node.props:
            state.AddInt(self._node, 'align-size', 4)

        super().ReadNode()

    def ReadBlobContents(self):
        data = self.ReadFileContents(self._pathname)
        self.header.length = sizeof(CsfHeader) + len(data)
        self.SetContents(bytes(self.header) + data)
        return True
