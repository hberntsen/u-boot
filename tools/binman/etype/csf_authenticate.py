# SPDX-License-Identifier: GPL-2.0+
# Written by Harm Berntsen <harm.berntsen@nedap.com>

"""Entry-type module for the key insertion command in a CSF file

This generates the key installation command data for a CSF file
"""

from binman.etype.csf_header import CsfHeader
from binman.etype.csf_authenticate_attachment import GetDataToAuthenticate
from binman.entry import Entry
from dtoc import fdt_util
from ctypes import *

HAB_CMD_AUT_DAT = 0xca
HAB_ENG_CAAM = 0x1d
HAB_PCL_CMS = 0xc5

class HabRegion(BigEndianStructure):
    _pack_ = 1
    _fields_ = [
            ("address", c_uint32),
            ("size", c_uint32),
            ]

class HabCmdAutDat(BigEndianStructure):
    _pack_ = 1
    _fields_ = [
            ("header", CsfHeader),
            ("key", c_uint8),
            ("sig_fmt", c_uint8),
            ("engine", c_uint8),
            ("eng_cfg", c_uint8),
            ("sig_loc", c_uint32)
            # followed by HabRegion instances (and add them to header.length)
            ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.header.tag = HAB_CMD_AUT_DAT
        self.header.length = sizeof(self)
        self.regions = []

    def AddRegion(self, region: HabRegion):
        self.regions.append(region)
        self.header.length += sizeof(HabRegion)

    def GetBytes(self):
        ret = b''.join([bytes(self)] + list(map(bytes, self.regions)))
        return ret

class Entry_csf_authenticate(Entry):
    """CSF authentication command

    This Entry generates authentication commands for within a CSF. For
    example, for authenticating a CSF::

        csf_install_csfk: csf_install_key@0 {
            tgt-index = <1>;
            attachment = <&csf_install_csfk_attachment>;
            csfk;
        };

        csf_authenticate_csf: csf_authenticate@0 {
            key-index = <1>;
            attachment = <&csf_authenticate_csf_attachment>;
        };

        ...

        csf_authenticate_csf_attachment: csf_authenticate_attachment_keyfile@0 {
            crt = "/cst-3.3.1/crts/CSF1_1_sha256_4096_65537_v3_usr_crt.der";
            key = "/cst-3.3.1/keys/CSF1_1_sha256_4096_65537_v3_usr_key.der";
            sign = <
                &csf_header
                &csf_install_srk
                &csf_install_csfk
                &csf_authenticate_csf
                &csf_install_image_key
                &csf_authenticate_spl
                &csf_unlock_caam_rng
            >;
        };

    Properties / Entry arguments:
        - key-index
            index at which the key used in the attachment has been loaded
            into. Defaults to 0.
        - attachment: phandle to the attachment Entry
        - sign
            List of phandles to sign. Leave empty to authenticate the CSF
            itself. Default: empty

    This entry must be at the start of the CSF section."""

    def ReadNode(self):
        super().ReadNode()

        self.attachment_phandle = fdt_util.GetPhandleList(self._node, 'attachment')
        if not self.attachment_phandle or not self.attachment_phandle[0]:
            self.Raise("Missing attachment phandle")
        self.attachment = self.attachment_phandle[0]
        self.sign = fdt_util.GetPhandleList(self._node, 'sign') or []
        self.key_index = fdt_util.GetInt(self._node, 'key-index', 0)

    def GenerateCmd(self) -> HabCmdAutDat:
        cmd = HabCmdAutDat()
        cmd.sig_fmt = HAB_PCL_CMS
        cmd.engine = HAB_ENG_CAAM
        cmd.key = self.key_index 
        attachment_entry = self.section.GetEntryByPhandle(self.attachment, self)
        cmd.sig_loc = attachment_entry.offset or 0

        self.missing = False
        for (load, data) in GetDataToAuthenticate(self, self.sign):
            if load is None or data is None:
                self.missing = True
            else:
                cmd.AddRegion(HabRegion(load, len(data)))
                self.Detail(f"Authenticating region load=0x{load:x}, "
                        f"len=0x{len(data):x}")

        return cmd

    def ObtainContents(self):
        cmd = self.GenerateCmd()
        self.SetContents(cmd.GetBytes())
        return True

    def ProcessContents(self):
        updated_data = self.GenerateCmd().GetBytes()
        return self.ProcessContentsUpdate(updated_data)
