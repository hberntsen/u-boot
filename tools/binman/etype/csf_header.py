# SPDX-License-Identifier: GPL-2.0+
# Written by Harm Berntsen <harm.berntsen@nedap.com>

"""Entry-type module for a CSF header, containing the length and version of
the CSF"""

from binman.entry import Entry
from dtoc import fdt_util
from ctypes import *

HAB_TAG_CSF = 0xd4

class CsfHeader(BigEndianStructure):
    _pack_ = 1
    _fields_ = [
            ("tag", c_uint8),
            ("length", c_uint16),
            ("par", c_uint8)
            ]

class Entry_csf_header(Entry):
    """An entry which contains the CSF header

    Properties / Entry arguments: 
        - hab-version
            byte that indicates the HAB version used (0xVv with V the major
            version and v the minor version). Defaults to 0x45
        - commands
            list of phandles that together cover all commands in this CSF
            (excluding their attachments). Used for calculating the length of
            the CSF.

    This entry must be at the start of the CSF section. For example, for a
    imx8mn based platform::

        ivt: imx8m_ivt {
            entrypoint = <CONFIG_SPL_TEXT_BASE>;
            loader = <&spl_ddr>;
            csf = <&csf>;
        };

        spl_ddr: section@0 {
            filename = "u-boot-spl-ddr.bin";
            pad-byte = <0xff>;
            align-size = <4>;
            align = <4>;

            u-boot-spl {
                align-end = <4>;
            };

            /* place ddr firmware blobs here */
        };

        csf: section@1 {
            csf_header: csf_header {
                commands = <
                    &csf_install_srk
                    &csf_install_csfk
                    &csf_authenticate_csf
                    &csf_install_image_key
                    &csf_authenticate_spl
                    &csf_unlock_caam_rng
                >;
            };

            csf_install_srk: csf_install_srk {
                src-index = <0>;
                attachment = <&csf_install_srk_attachment>;
            };

            csf_install_csfk: csf_install_key@0 {
                tgt-index = <1>;
                attachment = <&csf_install_csfk_attachment>;
                csfk;
            };

            csf_authenticate_csf: csf_authenticate@0 {
                key-index = <1>;
                attachment = <&csf_authenticate_csf_attachment>;
            };

            csf_install_image_key: csf_install_key@1 {
                tgt-index = <2>;
                attachment = <&csf_install_image_key_attachment>;
            };

            csf_authenticate_spl: csf_authenticate@1 {
                key-index = <2>;
                sign = <&ivt &spl_ddr>;
                attachment = <&csf_authenticate_spl_attachment>;
            };

            csf_unlock_caam_rng: csf_unlock {
                engine = [1d];
                args = [00 00 00 02];
            };

            csf_install_srk_attachment: blob-ext@0 {
                filename = "SRK_1_2_3_4_table.bin";
                // All CSF attachments need this alignment
                align-size = <4>;
            };

            csf_install_csfk_attachment: csf_file_attachment@1 {
                tag = [d7];
                filename = "CSF1_1_sha256_4096_65537_v3_usr_crt.der";
            };

            csf_authenticate_csf_attachment: csf_authenticate_attachment_keyfile@0 {
                crt = "CSF1_1_sha256_4096_65537_v3_usr_crt.der";
                key = "CSF1_1_sha256_4096_65537_v3_usr_key.der";
                sign = <
                    &csf_header
                    &csf_install_srk
                    &csf_install_csfk
                    &csf_authenticate_csf
                    &csf_install_image_key
                    &csf_authenticate_spl
                    &csf_unlock_caam_rng
                >;
            };

            csf_install_image_key_attachment: csf_file_attachment@2 {
                tag = [d7];
                filename = "IMG1_1_sha256_4096_65537_v3_usr_crt.der";
            };

            csf_authenticate_spl_attachment: csf_authenticate_attachment_keyfile@1 {
                crt = "IMG1_1_sha256_4096_65537_v3_usr_crt.der";
                key = "IMG1_1_sha256_4096_65537_v3_usr_key.der";
                sign = <&ivt &spl_ddr>;
            };
        };

    In this example, the keys and CSF are generated using NXP's CST. This tool
    can be downloaded from:
    https://www.nxp.com/webapp/Download?colCode=IMX_CST_TOOL_NEW"""
    def __init__(self, section, etype, node):
        super().__init__(section, etype, node)
        self.header = CsfHeader()
        self.header.tag = HAB_TAG_CSF

    def ReadNode(self):
        super().ReadNode()
        self.header.par = fdt_util.GetByte(self._node, 'hab-version', 0x45)
        self.commands = fdt_util.GetPhandleList(self._node, 'commands') or []

    def _GetLength(self):
        """Calculates the total length of the commands section of the CSF"""
        ents = [self.section.GetEntryByPhandle(p, self) for p in self.commands]
        return sizeof(CsfHeader) + sum([e.size or 0 for e in ents])

    def ObtainContents(self):
        self.header.length = self._GetLength()
        self.SetContents(bytes(self.header))
        return True

    def ProcessContents(self):
        self.header.length = self._GetLength()
        return self.ProcessContentsUpdate(bytes(self.header))
