# SPDX-License-Identifier: GPL-2.0+
# Written by Harm Berntsen <harm.berntsen@nedap.com>

"""Entry-type module for the CSF Unlock command"""

from binman.entry import Entry
from dtoc import fdt_util
from ctypes import sizeof
from binman.etype.csf_header import CsfHeader

HAB_CMD_UNLK = 0xB2

class Entry_csf_unlock(Entry):
    """CSF unlock command

    Properties / Entry arguments:
        - engine: byte that indicates the engine to be unlocked
        - args:
            (when required) byte array that contains the parameters for the
            engine

    For possible values, see the High Assurance Boot Version 4 Application
    Programming Interface Reference Manual (HAB4_API.pdf), 4.3.6 Unlock
    section. This manual is included with NXP's Code Signing Tool (CST)"""
    def __init__(self, section, etype, node):
        super().__init__(section, etype, node)
        self.header = CsfHeader()
        self.header.tag = HAB_CMD_UNLK

    def ReadNode(self):
        super().ReadNode()

        engine_byte = fdt_util.GetByte(self._node, 'engine')
        if not engine_byte:
            self.Raise("Missing engine byte")
        self.header.par = engine_byte

        args_prop = self._node.props.get('args')
        if args_prop:
            self.args = args_prop.bytes
        else:
            self.args = []

        self.header.length = sizeof(CsfHeader) + len(self.args)

    def ObtainContents(self):
        self.SetContents(bytes(self.header) + self.args)
        return True
