# SPDX-License-Identifier: GPL-2.0+
# Written by Harm Berntsen <harm.berntsen@nedap.com>

"""Entry-type module for installing the SRK table
"""

from binman.etype.csf_install_key import Entry_csf_install_key
from dtoc import fdt_util

HAB_PCL_SRK = 0x03
HAB_ALG_SHA256 = 0x17

class Entry_csf_install_srk(Entry_csf_install_key):
    """CSF command to install the SRK table

    Properties / Entry arguments:
        - attachment: phandle to the attachment Entry
        - src-index: the index of the key in the SRK table to load (range 0-3)

    This entry must immediately follow the CSF header"""
    def __init__(self, section, etype, node):
        super().__init__(section, etype, node)
        self.cmd.cert_fmt = HAB_PCL_SRK
        self.cmd.hash_alg = HAB_ALG_SHA256

    def ReadNode(self):
        super().ReadNode()
        src_index = fdt_util.GetInt(self._node, 'src-index')
        if src_index == None:
            self.Raise("property src-index not set")
        if src_index < 0 or src_index > 3:
            self.Raise("src-index out of range, valid range: [0,3]")
        self.cmd.src_index = src_index
