// SPDX-License-Identifier: GPL-2.0+
/*
 * Simple program to test the insertion of the fdt sha256sum. This is used by binman tests.
 */

#define CONFIG_BINMAN
#include <binman_sym.h>

binman_sym_declare(unsigned char, fit, fdt_sha256[32]);
